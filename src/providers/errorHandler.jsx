import { toast } from 'react-toastify';
import { HttpPost } from '../providers/httpClient.jsx';

let message;

const ErrorHandler = error => {
  if (error.code === 'ECONNABORTED') {
    //timout error
    message = 'Batas waktu habis. Silahkan coba lagi.';
    // let newData = {
    //   accountId: sessionStorage.getItem('katalis_activeAccount')
    //     ? JSON.parse(sessionStorage.getItem('katalis_activeAccount'))
    //         .accounts[0].id
    //     : '',
    //   path: error.error ? error.error.path : '',
    //   httpStatus: error.error ? error.error.status : '',
    //   requestMessage: '',
    //   responseMessage: error.error ? error.error.message : ''
    // };

    // HttpPost('log/error', newData);
  } else if (error.response) {
    // The request was made and the server responded with a status code
    // that falls out of the range of 2xx
    // console.log(error.response.data);
    // console.log(error.response.status);
    // console.log(error.response.headers);
    // if (error.response.data) {
    //   alert(error.response.data);
    // } else if (error.response.status) {
    //   alert(error.response.status);
    // } else if (error.response.headers) {
    //   alert(error.response.headers);
    // }
    if (error.response.status == 401) {
      message = 'Username dan/atau password salah';
      // let newData = {
      //   accountId: sessionStorage.getItem('katalis_activeAccount')
      //     ? JSON.parse(sessionStorage.getItem('katalis_activeAccount'))
      //         .accounts[0].id
      //     : '',
      //   path: error.error ? error.error.path : '',
      //   httpStatus: error.error ? error.error.status : '',
      //   requestMessage: '',
      //   responseMessage: error.error ? error.error.message : ''
      // };

      // HttpPost('log/error', newData);
    } else {
      // console.log(error);
      // console.log(error.response.data);
      // message = 'Terjadi kesalahan sistem';
      message = error.response.data.message;
      let newData = {
        accountId: sessionStorage.getItem('katalis_activeAccount')
          ? JSON.parse(sessionStorage.getItem('katalis_activeAccount'))
              .accounts[0].id
          : '',
        path: error.response.data.path,
        httpStatus: error.response.data.status,
        requestMessage: '',
        responseMessage: error.response.data.message
      };

      // HttpPost('log/error', newData);
    }
  } else if (error.request) {
    // console.log(error.request);
    // The request was made but no response was received
    // `error.request` is an instance of XMLHttpRequest in the
    // browser and an instance of
    // http.ClientRequest in node.js
    // console.log(error.request);
    message = 'Terjadi kesalahan sistem';
    let newData = {
      accountId: sessionStorage.getItem('katalis_activeAccount')
        ? JSON.parse(sessionStorage.getItem('katalis_activeAccount'))
            .accounts[0].id
        : '',
      path: error.error ? error.error.path : '',
      httpStatus: error.error ? error.error.status : '',
      requestMessage: '',
      responseMessage: error.error ? error.error.message : ''
    };

    // HttpPost('log/error', newData);
  } else {
    console.log('2');

    // Something happened in setting up the request that triggered an Error
    // console.log('Error', error.message);
    message = 'Terjadi kesalahan sistem';
    // let newData = {
    //   accountId: sessionStorage.getItem('katalis_activeAccount')
    //     ? JSON.parse(sessionStorage.getItem('katalis_activeAccount'))
    //         .accounts[0].id
    //     : '',
    //   path: error.error ? error.error.path : '',
    //   httpStatus: error.error ? error.error.status : '',
    //   requestMessage: '',
    //   responseMessage: error.error ? error.error.message : ''
    // };

    // HttpPost('log/error', newData);
  }
  toast.error(message);
  // console.log(error.config);
};

export default ErrorHandler;
