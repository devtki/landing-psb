import Axios from 'axios';
import { Promise } from 'q';
import ErrorHandler from './errorHandler.jsx';

let apiUrl = '';

if (window.location.hostname === 'sundev.duckdns.org') {
  apiUrl = process.env.API_URL2;
} else {
  apiUrl = process.env.API_URL1;
}

const timeout = 10000;
const httpClient = Axios.create();
httpClient.defaults.timeout = timeout;

const HttpLogin = (url, data) => {
  return new Promise((resolve, reject) => {
    httpClient
      .post(apiUrl + url, data)
      .then(res => {
        resolve(res);
      })
      .catch(error => {
        ErrorHandler(error);
        reject();
      });
  });
};

const HttpPost = (url, data) => {
  return new Promise((resolve, reject) => {
    httpClient
      .post(apiUrl + url, data, {
        headers: {
          Authorization: sessionStorage.getItem('katalis_token')
        }
      })
      .then(res => {
        resolve(res);
      })
      .catch(error => {
        resolve(error);
        ErrorHandler(error);
        reject();
      });
  });
};

const HttpPostFile = (url, data) => {
  return new Promise((resolve, reject) => {
    httpClient
      .post(apiUrl + url, data, {
        headers: {
          Authorization: sessionStorage.getItem('katalis_token'),
          'Content-Type': 'multipart/form-data'
        }
      })
      .then(res => {
        resolve(res);
      })
      .catch(error => {
        ErrorHandler(error);
        reject();
      });
  });
};

const HttpGet = url => {
  return new Promise((resolve, reject) => {
    httpClient
      .get(apiUrl + url, {
        headers: {
          Authorization: sessionStorage.getItem('katalis_token')
        }
      })
      .then(res => {
        resolve(res);
      })
      .catch(error => {
        ErrorHandler(error);
        reject();
      });
  });
};

const HttpPut = (url, data) => {
  return new Promise((resolve, reject) => {
    httpClient
      .put(apiUrl + url, data, {
        headers: {
          Authorization: sessionStorage.getItem('katalis_token')
        }
      })
      .then(res => {
        resolve(res);
      })
      .catch(error => {
        ErrorHandler(error);
        reject();
      });
  });
};

const HttpPutWithToken = (url, data, token) => {
  return new Promise((resolve, reject) => {
    httpClient
      .put(apiUrl + url, data, {
        headers: {
          Authorization: token
        }
      })
      .then(res => {
        resolve(res);
      })
      .catch(error => {
        ErrorHandler(error);
        reject();
      });
  });
};

const HttpDelete = url => {
  return new Promise((resolve, reject) => {
    httpClient
      .delete(apiUrl + url, {
        headers: {
          Authorization: sessionStorage.getItem('katalis_token')
        }
      })
      .then(res => {
        resolve(res);
      })
      .catch(error => {
        ErrorHandler(error);
        reject();
      });
  });
};

export {
  HttpLogin,
  HttpPost,
  HttpPostFile,
  HttpGet,
  HttpPut,
  HttpPutWithToken,
  HttpDelete
};
