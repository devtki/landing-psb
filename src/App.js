import React from 'react';
import { Link, animateScroll as scroll } from 'react-scroll';
import logo from './assets/logo.png';
import bg from './assets/bg.jpg';
import bg2 from './assets/bg2.jpg';
import bg3 from './assets/bg3.jpg';
import alur from './assets/alur.png';
import './App.css';
import { Parallax } from 'react-parallax';
import 'bootstrap/dist/css/bootstrap.min.css';
import { ToastContainer, toast } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';
import {
  Collapse,
  Navbar,
  NavbarToggler,
  NavbarBrand,
  Nav,
  Button,
  NavItem,
  NavLink,
  Row,
  Col,
  Modal,
  ModalHeader,
  ModalBody,
  Form,
  FormGroup,
  Label,
  Input,
  ModalFooter,
  FormFeedback
} from 'reactstrap';
import { HttpPost } from './providers/httpClient.jsx';

class App extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      modal: false,
      toggle: false,
      valid: {
        name: true,
        phone: true,
        password: true,
        reTypePassword: true
      },
      invalid: {
        name: '',
        phone: '',
        password: '',
        reTypePassword: ''
      }
    };
    this.modal = this.modal.bind(this);
    this.submit = this.submit.bind(this);
    this.toggle = this.toggle.bind(this);
    // this.validate = this.validate(this);
  }

  toggle() {
    this.setState({
      toggle: !this.state.toggle
    });
  }

  modal() {
    this.setState({ modal: !this.state.modal });
  }

  validate(event) {
    console.log('this', event);

    let valid = {
      name: true,
      phone: true,
      password: true,
      reTypePassword: true
    };
    if (!event.target.name.value) {
      valid.name = false;
      this.setState(prevState => ({
        invalid: {
          ...prevState.invalid,
          name: 'Nama Harap Diisi'
        }
      }));
    }
    if (!event.target.phone.value) {
      valid.phone = false;

      this.setState(prevState => ({
        invalid: {
          ...prevState.invalid,
          phone: 'No. Telepon Harap Diisi'
        }
      }));
    }
    if (!event.target.password.value) {
      valid.password = false;

      this.setState(prevState => ({
        invalid: {
          ...prevState.invalid,
          password: 'Password Harus Diisi'
        }
      }));
    }
    if (!event.target.reTypePassword.value) {
      valid.reTypePassword = false;

      this.setState(prevState => ({
        invalid: {
          ...prevState.invalid,
          reTypePassword: 'Konfirmasi Password'
        }
      }));
    }
    this.setState({
      valid: valid
    });
    return valid.name && valid.phone && valid.password && valid.reTypePassword;
  }

  submit(event) {
    event.preventDefault();

    if (!this.validate(event)) {
      return;
    }
    let companyId = '5ce787c76dbfaa0ae475bdb6';
    let name = event.target.name.value;
    let dateOfBirth = event.target.dateOfBirth.value;
    let address = event.target.address.value;
    let email = event.target.email.value;
    let phone = event.target.phone.value;
    let password = event.target.password.value;
    let reTypePassword = event.target.reTypePassword.value;
    let data = {
      companyId: companyId,
      name: name,
      dateOfBirth: dateOfBirth,
      address: address,
      email: email,
      phone: phone,
      password: password,
      reTypePassword: reTypePassword
    };
    HttpPost('master/customer/admin/register', data).then(res => {
      console.log(res);
      this.modal();
      toast(
        <div>
          <h6>Registrasi Telah Berhasil</h6>
          <hr />
          <small>
            Silahkan Gunakan <b>E-Mail/Nomor Telepon dan Password</b> Untuk
            Login
          </small>
        </div>
      );
    });
  }

  render() {
    const styles = {
      fontFamily: 'sans-serif',
      textAlign: 'center'
    };
    const insideStyles = {
      background: 'white',
      width: '100%',
      padding: 20,
      position: 'absolute',
      top: '50%',
      left: '50%',
      transform: 'translate(-50%,-50%)',
      opacity: 0.8
    };
    const image1 =
      'https://images.unsplash.com/photo-1498092651296-641e88c3b057?auto=format&fit=crop&w=1778&q=60&ixid=dW5zcGxhc2guY29tOzs7Ozs%3D';
    const image2 =
      'https://img00.deviantart.net/2bd0/i/2009/276/c/9/magic_forrest_wallpaper_by_goergen.jpg';
    const image3 =
      'https://brightcove04pmdo-a.akamaihd.net/5104226627001/5104226627001_5297440765001_5280261645001-vs.jpg?pubId=5104226627001&videoId=5280261645001';
    const image4 =
      'https://images.fineartamerica.com/images/artworkimages/mediumlarge/1/empire-state-building-black-and-white-square-format-john-farnan.jpg';

    return (
      <div>
        <div style={styles}>
          <Modal className="op" isOpen={this.state.toggle} toggle={this.toggle}>
            <ModalBody>
              <img src={alur} />
            </ModalBody>
          </Modal>
          <Modal
            // style={{ marginTop: '90px' }}
            isOpen={this.state.modal}
            toggle={this.modal}
          >
            <Form onSubmit={this.submit}>
              <ModalHeader>Registrasi Akun PPDB Online</ModalHeader>
              <ModalBody>
                <FormGroup>
                  <Label for="name">Nama Wali Siswa</Label>
                  <Input
                    type="text"
                    name="name"
                    id="name"
                    placeholder="Masukkan Nama Lengkap"
                    invalid={!this.state.valid.name}
                  ></Input>
                  <FormFeedback>
                    <small>{this.state.invalid.name}</small>
                  </FormFeedback>
                </FormGroup>
                <FormGroup>
                  <Label for="name">Nomor Telepon</Label>
                  <Input
                    type="text"
                    name="phone"
                    id="phone"
                    min="0"
                    placeholder="Masukkan Nomor Telepon"
                    invalid={!this.state.valid.phone}
                  ></Input>
                  <FormFeedback>
                    <small>{this.state.invalid.phone}</small>
                  </FormFeedback>
                </FormGroup>
                <FormGroup>
                  <Label for="name">Alamat E-Mail</Label>
                  <Input
                    type="email"
                    name="email"
                    id="email"
                    placeholder="Masukkan Alamat E-Mail"
                  ></Input>
                </FormGroup>
                <FormGroup>
                  <Label for="address">Alamat Tempat Tinggal</Label>
                  <Input
                    type="textarea"
                    name="address"
                    placeholder="Masukkan Alamat Tempat Tinggal"
                  ></Input>
                </FormGroup>
                <FormGroup>
                  <Label for="dateOfBirth">Tanggal Lahir</Label>
                  <Input type="date" name="dateOfBirth"></Input>
                </FormGroup>
                <FormGroup>
                  <Label for="password">Password</Label>
                  <Input
                    type="password"
                    name="password"
                    id="password"
                    placeholder="Masukkan Password"
                    invalid={!this.state.valid.password}
                  ></Input>
                  <FormFeedback>
                    <small>{this.state.invalid.password}</small>
                  </FormFeedback>
                </FormGroup>
                <FormGroup>
                  <Label for="password">Konfirmasi Password</Label>
                  <Input
                    type="password"
                    name="reTypePassword"
                    id="reTypePassword"
                    placeholder="Konfirmasi Password"
                    invalid={!this.state.valid.reTypePassword}
                  >
                    <FormFeedback>
                      <small>{this.state.invalid.reTypePassword}</small>
                    </FormFeedback>
                  </Input>
                </FormGroup>
              </ModalBody>
              <ModalFooter>
                <FormGroup>
                  <Button
                    type="button"
                    color="warning"
                    className="mr-2"
                    onClick={this.modal}
                  >
                    Batal
                  </Button>
                  <Button color="primary" type="submit">
                    Simpan
                  </Button>
                </FormGroup>
              </ModalFooter>
            </Form>
          </Modal>
          <Navbar
            fixed
            dark
            color="dark"
            // className="navbar-fixed-top"
            style={{ position: 'fixed', top: 0, zIndex: 1000, width: '100%' }}
          >
            <NavbarBrand className="text-inverse">
              <h5>PPDB Online Insan Cendekia Payakumbuh</h5>
            </NavbarBrand>
            <Nav>
              <NavItem>
                <NavLink>
                  <Link
                    // href="#welcome"
                    className="text-light noselect"
                    activeClass="active"
                    to="welcome"
                    spy={true}
                    smooth={true}
                    offset={-70}
                    duration={500}
                  >
                    Welcome
                  </Link>
                </NavLink>
              </NavItem>
              <NavItem>
                <NavLink>
                  <Link
                    // href="#informasi"
                    className="text-light noselect"
                    activeClass="active"
                    to="informasi"
                    spy={true}
                    smooth={true}
                    offset={-70}
                    duration={500}
                  >
                    Informasi PPDB
                  </Link>
                </NavLink>
              </NavItem>
              <NavItem>
                <NavLink>
                  <Link
                    // href="#pendaftaran"
                    className="text-light noselect"
                    activeClass="active"
                    to="pendaftaran"
                    spy={true}
                    smooth={true}
                    offset={-70}
                    duration={500}
                  >
                    Registrasi
                  </Link>
                </NavLink>
              </NavItem>
            </Nav>
          </Navbar>
          <div>
            <div id="welcome">
              <ToastContainer autoClose={false} />
              <Parallax
                className="text-center"
                bgImage={bg}
                blur={{ min: 1, max: 2 }}
              >
                <div style={{ height: '100vh' }}>
                  <div style={insideStyles}>
                    <h4 style={{ opacity: 1 }}>
                      Selamat Datang di Portal PPDB Online
                    </h4>
                    <Row className="mb-2" style={{ margin: 'auto' }}>
                      <Col>
                        <img src={logo} width="170px"></img>
                        {/* <h3>
                        السَّلاَمُ عَلَيْكُمْ وَرَحْمَةُ اللهِ وَبَرَكَاتُهُ
                      </h3> */}
                      </Col>
                    </Row>
                    <h1>Insan Cendekia Payakumbuh</h1>
                  </div>
                </div>
              </Parallax>
            </div>
            <hr />
            <div id="informasi">
              <Parallax
                className="text-center"
                bgImage={bg2}
                blur={{ min: 1, max: 3 }}
              >
                <div style={{ height: '100vh' }}>
                  <div style={insideStyles}>
                    <Row>
                      <Col>
                        <h3>Tahap Pendaftaran</h3>
                        <hr />
                        <p>
                          <strong>Waktu Pendaftaran</strong> <br />
                          <small>1 September 2019 s/d 25 Desember 2019</small>
                          <br />
                          <strong>Waktu Tes</strong> <br />
                          <small>Ahad, 29 Desember 2019</small>
                          <br />
                          <strong>Pengumuman</strong> <br />
                          <small>Sabtu, 3 Januari 2020</small>
                        </p>
                      </Col>
                      <Col>
                        <h3>Tes Meliputi</h3>
                        <hr />
                        <div>
                          <strong className="mb-3">- Tes Tertulis</strong>
                          <br />
                          <strong className="mb-3">- Psikotes</strong>
                          <br />
                          <strong className="mb-3">
                            - Tes Bacaan dan Hafalan Al Qur'an
                          </strong>
                          <br />
                          <strong className="mb-3">
                            - Tes wawancara orang tua
                          </strong>
                        </div>
                      </Col>
                      <Col>
                        <h3>Syarat Pendaftaran</h3>
                        <hr />
                        <div
                          className="mb-1"
                          style={{
                            // position: 'absolute',
                            maxHeight: 300,
                            overflowY: 'auto',
                            overflowX: 'hidden'
                          }}
                        >
                          <div>
                            <p>
                              - Foto copy rapor Hasil belajar <br />
                              <strong>
                                (Kelas I s/d VI SD bagi pendaftar SMP)
                              </strong>
                              <br />
                              <strong>
                                (Kelas VII s/d IX bagi pendaftar SMA)
                              </strong>
                            </p>
                          </div>
                          <div className="mb-1">
                            <p>
                              - Pas foto ukuran 3x4 dan 2x3 masing-masing
                              sebanyak 2 lembar
                            </p>
                          </div>
                          <div className="mb-1">
                            <p>- Fotocopy Akte Kelahiran anak</p>
                          </div>
                          <div className="mb-1">
                            <p>- Fotocopy Kartu NISN anak</p>
                          </div>
                          <div className="mb-1">
                            <p>- Fotocopy KK</p>
                          </div>
                          <div className="mb-1">
                            <p>- Fotocopy KTP orang tua</p>
                          </div>
                          <div className="mb-1">
                            <p>
                              - Membayar uang pendaftaran sejumlah Rp.350.000
                            </p>
                          </div>
                          <div className="mb-1">
                            <p>
                              - Mengisi form pendaftaran calon peserta didik
                              baru
                            </p>
                          </div>
                          <div className="mb-1">
                            <p>- Mengisi form angket bagi orang tua</p>
                          </div>
                          <div className="mb-1">
                            <p>
                              - Melampirkan slip setoran sebagai bukti
                              pembayaran
                            </p>
                          </div>
                          <div className="mb-1">
                            <p>
                              - Untuk pendaftar online, silahkan mengisi form
                              dan mengupload administrasi yg telah ditentukan
                            </p>
                          </div>
                        </div>
                      </Col>
                    </Row>
                  </div>
                </div>
              </Parallax>
            </div>
            <hr />
            <div id="pendaftaran" className="mb-0">
              <Parallax bgImage={bg3} blur={{ min: 1, max: 3 }}>
                <div style={{ height: '100vh' }}>
                  <div style={insideStyles}>
                    <Row>
                      <Col>
                        <h3>Tata Cara Pendaftaran</h3>
                        <hr />
                        <div>
                          <small>
                            - Klik tombol <b>Tampilkan Alur Registrasi</b> untuk
                            melihat detail alur pendaftaran PPDB Online ICBS
                            Payakumbuh
                          </small>
                        </div>
                        <div>
                          <small>
                            - Klik tombol{' '}
                            <b>Registrasi Sistem PPDB ICBS Payakumbuh</b>
                          </small>
                        </div>
                        <div>
                          <small>
                            - Isikan form Registrasi Akun Sistem PPDB Online
                            secara lengkap & benar.{' '}
                          </small>
                        </div>
                        <div>
                          <small>
                            - Simpan & ingat baik-baik{' '}
                            <b>e-mail/nomor telepon,</b> dan<b> password</b>{' '}
                            untuk keperluan{' '}
                            <b>
                              login di sistem PPDB Online Insan Cendekia
                              Payakumbuh.
                            </b>
                          </small>
                        </div>
                        <div>
                          <small>
                            - Setelah Anda mendapatkan notifikasi bahwa akun
                            Anda telah berhasil dibuat, klik
                            <br />
                            <b>Login Sistem PPDB ICBS Payakumbuh</b>
                          </small>
                        </div>
                      </Col>
                      <Col style={{ margin: 'auto', zIndex: 9999 }}>
                        <Row>
                          <Col>
                            <Button
                              onClick={this.modal}
                              className="mb-2"
                              size="lg"
                              color="success"
                            >
                              Registrasi Sistem PPDB Online ICBS Payakumbuh
                            </Button>
                          </Col>
                        </Row>
                        <Row>
                          <Col>
                            <a
                              href="https://psb.solusinegeri.com/icbspayakumh"
                              target="_blank"
                            >
                              <Button size="lg" color="primary">
                                Login Sistem PPDB ICBS Payakumbuh
                              </Button>
                            </a>
                          </Col>
                        </Row>
                      </Col>
                    </Row>
                  </div>
                </div>
              </Parallax>
            </div>
          </div>
        </div>
      </div>
    );
  }
}
export default App;
